<?php
use App\Http\Controllers\BotManController;
use App\Http\Controllers\OmsetController;
use App\Http\Controllers\SearchBarang;
use BotMan\BotMan\Messages\Attachments\Image;
//use Config;
//use Illuminate\Support\Facades\Storage;

$botman = resolve('botman');

$botman->hears('Hi', function ($bot) {
    $bot->reply('Hello!');
 });
//$botman->hears('halo|hello|hi|test|ping|p|mbak|mas|pak|bu', BotManController::class.'@startConversation');

$botman->group(['recipient' => config('telegram.groupid')], function($bot) {
    $bot->hears('/cari {pencarian}', SearchBarang::class.'@botSearch');
    $bot->hears('/kode {pencarian}', SearchBarang::class.'@botSearchCode');
    $bot->hears('/help', function ($bot) {
        $bot->reply("/cari => misalkan anda ingin mencari barang dengan kata kunci *kayu bulat*, maka ketik : */cari kayu bulat*\n\n/kode => apabila anda sudah mengetahui kode barang / barcode yang akan dicari, maka ketik : */kode 1ALMP0006* ( tanpa spasi )\n\n/help => menampilkan text bantuan ini.",[
            'parse_mode' => 'Markdown'
        ]);
    });
    
    $bot->hears('/omset', function($bot) {
        OmsetController::omset($bot);
    });

    $bot->receivesImages(function($bot, $images) {

        SearchBarang::botSearchBarCode($bot, $images);
    
        
    });


});

$botman->group(['recipient' => '-1001170800820'], function($bot) {
    $bot->hears('/mg {pencarian}', SearchBarang::class.'@botSearch');
    // $bot->hears('/kode {pencarian}', SearchBarang::class.'@botSearchCode');
    // $bot->hears('/help', function ($bot) {
    //     $bot->reply("/cari => misalkan anda ingin mencari barang dengan kata kunci *kayu bulat*, maka ketik : */cari kayu bulat*\n\n/kode => apabila anda sudah mengetahui kode barang / barcode yang akan dicari, maka ketik : */kode 1ALMP0006* ( tanpa spasi )\n\n/help => menampilkan text bantuan ini.",[
    //         'parse_mode' => 'Markdown'
    //     ]);
    // });

    // $bot->receivesImages(function($bot, $images) {

    //     SearchBarang::botSearchBarCode($bot, $images);
    
        
    // });


});

//$botman->hears('/kode {pencarian}', SearchBarang::class.'@botSearchCode');


$botman->hears('/find {pencarian}', SearchBarang::class.'@botSearch');


