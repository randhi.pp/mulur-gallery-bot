<?php

//use Config;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::match(['get', 'post'], '/'.config('telegram.webhooksecret').'/botman', 'BotManController@handle');
// Route::get('/botman/tinker', 'BotManController@tinker');
//Route::get('/botman/send', 'BotManController@send');
//Route::get('/barang/harga/{barang_id}', 'HargaController@cek');
//Route::get('/barang/search/{pencarian}', 'SearchBarang@search');

Route::get('/omset', 'OmsetController@omset');
