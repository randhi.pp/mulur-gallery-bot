<?php 
setLocale(LC_TIME, 'id_ID');

date_default_timezone_set('Asia/Jakarta');

include('connect.php');
include('vendor/autoload.php');

use \unreal4u\TelegramAPI\HttpClientRequestHandler;
use \unreal4u\TelegramAPI\TgLog;
use \unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use Carbon\Carbon;

function dd($data){
    echo '<pre>';
    die(var_dump($data));
    echo '</pre>';
}

\Carbon\Carbon::setLocale('id');
$date = Carbon::now();

//get data omset interval jam saat ini.
$tvar = array("keramik","cat","sanitary","building","hardware","lainlain","ongkir","ongkar");

$omsetperjam = [];
foreach ($tvar as $kategori) {

    $sql = "SELECT * FROM nota WHERE user LIKE 'kasir%' AND remarks NOT LIKE 'RTJ%' AND kategori = \"".$kategori."\"AND DATE(`timestamp`) = '".$date->toDateString()."' AND hour(`timestamp`) <= '". $date->format('G:i').":00' AND hour(`timestamp`) >= '".$date->subHour()->format('G:i').":00'" ;

    $date->addHour();
    //dd($sql);

    //$stmt = ;
    $sum = 0 ;
    //$stmt = ;
    foreach ($dbh->query($sql) as $data ) {
        //echo $row['total']."<br />\n";
        //dd($data['total']);
        $sum = $sum + $data['total'] ;
        
    }
    $omsetperjam[$kategori] = $sum ;
}

$returperjam = [];
foreach ($tvar as $kategori) {

    $sql = "SELECT * FROM nota WHERE user LIKE 'kasir%' AND remarks LIKE 'RTJ%' AND kategori = \"".$kategori."\"AND DATE(`timestamp`) = '".$date->toDateString()."' AND hour(`timestamp`) <= '". $date->format('G:i').":00' AND hour(`timestamp`) >= '".$date->subHour()->format('G:i').":00'" ;

    $date->addHour();
    //dd($sql);

    //$stmt = ;
    $sum = 0 ;
    //$stmt = ;
    foreach ($dbh->query($sql) as $data ) {
        //echo $row['total']."<br />\n";
        //dd($data['total']);
        $sum = $sum + $data['total'] ;
        
    }
    $returperjam[$kategori] = $sum ;
}


$omsettotal = [];
foreach ($tvar as $kategori) {

    $sql = "SELECT * FROM nota WHERE user LIKE 'kasir%' AND remarks NOT LIKE 'RTJ%' AND kategori = \"".$kategori."\"AND DATE(`timestamp`) = '".$date->toDateString()."'" ;

    //dd($sql);
    $sum = 0 ;
    //$stmt = ;
    foreach ($dbh->query($sql) as $data ) {
        //echo $row['total']."<br />\n";
        //dd($data['total']);
        $sum = $sum + $data['total'] ;
        
    }
    $omsettotal[$kategori] = $sum ;
    
}

$returtotal = [];
foreach ($tvar as $kategori) {

    $sql = "SELECT * FROM nota WHERE user LIKE 'kasir%' AND remarks LIKE 'RTJ%' AND kategori = \"".$kategori."\"AND DATE(`timestamp`) = '".$date->toDateString()."'" ;

    //dd($sql);
    $sum = 0 ;
    //$stmt = ;
    foreach ($dbh->query($sql) as $data ) {
        //echo $row['total']."<br />\n";
        //dd($data['total']);
        $sum = $sum + $data['total'] ;
        
    }
    $returtotal[$kategori] = $sum ;
    
}

$gt = 0;
$gt2 = 0;
//dd($omsetperjam);
$text = "*Laporan Penjualan Kasir - ASNET*\n\n";
$text .= $date->translatedFormat('l, d F Y')."\nJam ".$date->subHour()->format('G:i')." s/d ".$date->addHour()->format('G:i');

$text .= "\n\n💰 Penjualan 💰 ";
$text .= "\n----\n```\n";
foreach ($tvar as $kategori) {
   
    $text .= str_pad($kategori, 8)." : Rp ".str_pad(number_format($omsetperjam[$kategori],0,',','.'), 11, " ", STR_PAD_LEFT).",-\n"; 
}

$text .= "```\nRetur ";
$text .= "\n----\n```\n";
$gtreturperjam = 0 ;
foreach ($tvar as $kategori) {
    if ($returperjam[$kategori] != 0 ) {
        $text .= str_pad($kategori, 8)." : Rp ".str_pad(number_format($returperjam[$kategori],0,',','.'), 11, " ", STR_PAD_LEFT).",-\n";
    }
    $gtreturperjam += $returperjam[$kategori];
   // $text .= $kategori.": Rp ".number_format($returperjam[$kategori],2,',','.')."\n"; 
            
}
if ($gtreturperjam == 0 ) {
    $text .= "``` Tidak ada retur di jam ini.\n\n";
} else {
    $text .= "```\n\n";
}




$text .= "✅📊 Omset Harian 📊✅";
$text .= "\n---- ``` \n";
foreach ($tvar as $kategori) {
       
    $text .= str_pad($kategori, 8)." : Rp ".str_pad(number_format($omsettotal[$kategori],0,',','.'), 11, " ", STR_PAD_LEFT).",-\n";
   // $text .= $kategori.": Rp ".number_format($omsettotal[$kategori],2,',','.')."\n"; 
    $gt = $gt +  $omsettotal[$kategori] ;       
}
$text .= "```\nTotal Retur";
$text .= "\n----\n```\n";
$gtreturperjam2 = 0 ;
foreach ($tvar as $kategori) {

    if ($returtotal[$kategori] != 0 ){
        $text .= str_pad($kategori, 8)." : Rp ".str_pad(number_format($returtotal[$kategori],0,',','.'), 11, " ", STR_PAD_LEFT).",-\n";
    }
    $gtreturperjam2 += $returtotal[$kategori];
    
    //$text .= $kategori.": Rp ".number_format($returtotal[$kategori],2,',','.')."\n"; 
    $gt2 = $gt2 +  $returtotal[$kategori] ;        
}
if ($gtreturperjam2 == 0 ) {
    $text .= "``` Tidak ada retur di jam ini.\n";
} else {
    $text .= "```\n";
}

$text .= "\n=========================== ``` ";
$text .= "\nTotal    : Rp ".str_pad(number_format($gt,0,',','.'), 11, " ", STR_PAD_LEFT).",-";
$text .= "\nRetur    : Rp ".str_pad(number_format($gt2,0,',','.'), 11, " ", STR_PAD_LEFT).",-";
$text .= "\nGT       : Rp ".str_pad(number_format($gt+$gt2,0,',','.'), 11, " ", STR_PAD_LEFT).",-";
$text .= "```\n===========================";

//dd($text);

$loop = \React\EventLoop\Factory::create();
$handler = new HttpClientRequestHandler($loop);
$tgLog = new TgLog("906234092:AAGYoEypWEeRgJ-VDb09958FJDIDNUl_W24", $handler);

$sendMessage = new SendMessage();
$sendMessage->chat_id = '-377366836';
$sendMessage->parse_mode= 'Markdown';
$sendMessage->text = $text;

$tgLog->performApiRequest($sendMessage);
$loop->run();