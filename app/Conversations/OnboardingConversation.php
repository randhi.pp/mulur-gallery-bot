<?php

namespace App\Conversations;

use Validator;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use App\Controllers\HargaController;
//use Config;

class OnboardingConversation extends Conversation
{
    public function boturl() {
        return \Config::get('urlcek.boturl');
    }
    public function askName()
    {
        $question = Question::create("Terima Kasih sudah menghubungi Mulur Gallery, sebelumnya saya berbicara dengan Bapak/Ibu?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Bapak')->value('bapak'),
                Button::create('Ibu')->value('ibu'),
            ]);

        $this->ask($question, function (Answer $answer) {
            
                if ($answer->getValue() === 'bapak') {
                    $this->bot->userStorage()->save([
                        'gender' => 'Bapak',
                    ]);
                } else {
                    $this->bot->userStorage()->save([
                        'gender' => 'Ibu',
                    ]);
                }

                $this->askName2();   
            
        });

        
        
        
    }

    public function askName2()
    {
        $user = $this->bot->userStorage()->find();

        $this->gender = $user->get('gender');

        $this->ask('Baik dengan '. $this->gender.' siapa?', function(Answer $answer) {
            $this->bot->userStorage()->save([
                'name' => $answer->getText(),
            ]);
                
            $this->say('Baik '.  $this->gender .' '. $answer->getText() . ', perkenalkan saya Mona, customer service Mulur Gallery.');

            $this->askHelp();   
            
        });
        
    }

    public function askHelp()
    {
        $question = Question::create("Ada yang bisa saya bantu?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Cek Harga')->value('cekharga'),
                Button::create('Lainnya')->value('lainnya'),
            ]);

        $this->ask($question, function (Answer $answer) {
            
                if ($answer->getValue() === 'cekharga') {
                    $this->searchMethode();  

                } else {
                    
                }

                //$this->askName2();   
            
        });
    }

    public function searchMethode()
    {
        $question = Question::create("Input Kode Barang Atau Barcode?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create("Kode Barang | misal: 1 ALMP0006 / 9907908020003")->value('code'),
                Button::create("Cari | misal: Nippon")->value('search'),
            ]);

        $this->ask($question, function (Answer $answer) {
            
                if ($answer->getValue() === 'code') {
                    $this->bot->userStorage()->save([
                        'type' => 'code',
                        'usercode' => $answer->getText(),
                    ]);

                    $this->inputCode();   

                } else {
                    $this->bot->userStorage()->save([
                        'type' => 'search',
                        'usercode' => $answer->getText(),
                    ]);

                    $this->searchCode(); 
                }

                
            
        });
    }

    public function inputCode()
    {
        
        $user = $this->bot->userStorage()->find();

        

        $this->ask('Baik, anda memilih '. $user->get('type').', silahkan input kode... ', function(Answer $answer) {
            
            $ctrl = new \App\Http\Controllers\HargaController();
            $barang = $ctrl->cek($answer->getText());
            //$barang = HargaController::cekHarga($answer->getText());
            if ($barang->type != '404' ) {    
                $this->say("Berikut Info Barang yang anda inginkan :\n
                            \nNama Barang : ".$barang->value->nama.
                            "\nKode/Barcode : [".$barang->value->kode."] ".$barang->value->barcode.
                            "\n```Harga Jual : Rp ".number_format($barang->value->hargajual,0,',','.').",- / ".$barang->value->satuan."```"
                        ,
                        [
                            'parse_mode' => 'Markdown'
                        ]);

                } else {
                    $this->say("Whoops, tidak ada kode barang itu disini, cek kembali input anda!",[
                        'parse_mode' => 'Markdown'
                    ]);
                }
                
                //$this->askHelp();   
            
        });

        
    }

    public function searchCode()
    {
        
        $user = $this->bot->userStorage()->find();

        

        $this->ask('Baik, silahkan input nama barang', function(Answer $answer) {
            
            $ctrl = new \App\Http\Controllers\HargaController();
            $barang = $ctrl->search($answer->getText());
            //$barang = HargaController::cekHarga($answer->getText());
            if ($barang->type != '404' ) {
                $message = "Berikut Beberapa Info Barang yang sesuai dengan yang anda cari :\n" ;

                foreach ($barang->value as $result) {
                    $message .= "\nNama Barang : ".$result->nama.
                    "\nKode/Barcode : [".$result->kode."] ".$result->barcode.
                    "\n```Harga Jual : Rp ".number_format($result->hargajual,0,',','.').",- / ".$result->satuan."```" ;
                }
                

                $this->say($message,[
                    'parse_mode' => 'Markdown'
                ]);
            } else {
                $this->say("Whoops, tidak ada nama barang itu disini, cek kembali input anda!",[
                    'parse_mode' => 'Markdown'
                ]);
            }

            //$this->askHelp();   
            
        });

        
    }

    public function run()
    {
        $this->askName();
    }
}
