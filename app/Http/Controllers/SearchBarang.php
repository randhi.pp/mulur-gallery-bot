<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Attachments\Image;
use Illuminate\Support\Facades\Storage;

//use Config;

class SearchBarang extends Controller
{   
    
    public function botSearch($bot, $pencarian)
	{
        //$url = $this->boturl()."barang/search/".str_replace(' ', '-', $pencarian);
        //dd($url);

        $pencarian = str_replace(' ', '-', $pencarian);
        
        $ctrl = new \App\Http\Controllers\HargaController();
        $results = $ctrl->search($pencarian);
                
        //$results = json_decode($results);
                
        $user = $bot->getUser();
        $firstname = $user->getFirstName();
        $data = json_decode(json_encode($bot->getMessage()->getPayload()),false);
        if ($results->type == "success" ) {
            
            if ($results->count <= '50') {
                
                $message = "Halo *".$firstname."*,\nberikut hasil pencarian barang dengan query\n ``` ".$results->query." ```\n" ;

                foreach ($results->value as $result) {
                    $message .= "\n---\n*".$result->nama."*```\n".
                    "\nKode  : [".$result->kode."] ".$result->barcode.
                    "\nSaldo : ".$result->saldo." ".$result->satuan. " " . ($result->saldo > 0 ? "✅" : "❌") .
                    "\nHarga : Rp ".str_pad(number_format($result->hargajual,0,',','.'),13," ",STR_PAD_LEFT).",-/".$result->satuan."```" ;
                }

                $message .= "\n\nTotal ".$results->count." item ditemukan";
                $message .= "\n\n".$results->update;
                
                $bot->reply($message,[
                    'reply_to_message_id' => $data->message_id,
                    'parse_mode' => 'Markdown'
                ]);
            } else {
                $message = "Maaf, hasil pencarian tidak bisa ditampilkan, karena ditemukan ``` ".$results->count." item ``` \nMohon ubah kata kunci pencarian lebih spesifik.";

                $bot->reply($message,[
                    'reply_to_message_id' => $data->message_id,
                    'parse_mode' => 'MarkdownV2'
                ]);

            }
            
            
            
        
        } else {
                
            $bot->reply("Whoops, tidak ada nama barang itu disini, cek kembali input anda!",[
                'reply_to_message_id' => $data->message_id,
                'parse_mode' => 'MarkdownV2'
            ]);

        }

        

    }
    

    public static function botSearchBarCode($bot, $images)
	{
        //$url = $this->boturl()."barang/search/".str_replace(' ', '-', $pencarian);
        //dd($url);
        foreach ($images as $image) {

            $url = $image->getUrl(); // The direct url
            //$ext = pathinfo($url);
            $title = $image->getTitle(); // The title, if available
            $payload = $image->getPayload(); // The original payload
    
            Storage::put('telegram-images-test', file_get_contents($url) );
    
            $file_path = storage_path('app/telegram-images-test');
            
            $barcode = trim(exec(config('zbar.path').' -q --raw '.$file_path));
            
            if ($barcode != "") {
                $bot->reply("Anda mengirim foto dengan barcode : ".$barcode,
                    ['parse_mode' => 'MarkdownV2']
                );

                $pencarian = $barcode;
        
                $ctrl = new \App\Http\Controllers\HargaController();
                $results = $ctrl->cek($pencarian);
                        
                //$results = json_decode($results);
                $user = $bot->getUser();
                $firstname = $user->getFirstName();        
                
                
                if ($results->type == "success" ) {
                    
                    $message = "Halo *".$firstname."*, berikut hasil pencarian barang:\n" ;

                
                        $message .= "---\n".$results->value->nama.
                        "``` \nKode  : [".$results->value->kode."] ".$results->value->barcode.
                        "\nSaldo : ".$results->value->saldo." ".$results->value->satuan.
                        "\nHarga : Rp ".number_format($results->value->hargajual,0,',','.').",-/".$results->value->satuan."```" ;
                
                    

                    $bot->reply($message,[
                        'parse_mode' => 'MarkdownV2'
                    ]);
                
                } else {
                        
                    $bot->reply("Whoops, tidak ada nama barang itu disini, cek kembali input anda!",[
                        'parse_mode' => 'MarkdownV2'
                    ]);

                }
            } else {
                $bot->reply("Mohon Maaf barcode tidak ditemukan, kirim ulang foto dan pastikan barcode ada di bagian tengah foto dan dapat dengan jelas dilihat.\nAtau gunakan perintah:\n ``` /kode nomorbarcode ```",[
                    'parse_mode' => 'MarkdownV2'
                ]);
            }
                

           
    
      

        

        }

    }

    public function botSearchCode($bot, $pencarian)
	{
        //$url = $this->boturl()."barang/search/".str_replace(' ', '-', $pencarian);
        //dd($url);

        $pencarian = str_replace(' ', '-', $pencarian);
        
        $ctrl = new \App\Http\Controllers\HargaController();
        $results = $ctrl->cek($pencarian);
                
        //$results = json_decode($results);
        $user = $bot->getUser();
        $firstname = $user->getFirstName();        
        
        
        if ($results->type == "success" ) {
            
            $message = "Halo *".$firstname."*, berikut hasil pencarian barang:\n" ;

           
                $message .= "\n---\n".$results->value->nama.
                "``` \nKode: [".$results->value->kode."] ".$results->value->barcode.
                "\nHarga: Rp ".number_format($results->value->hargajual,0,',','.').",-/".$results->value->satuan."```" ;
           
            

            $bot->reply($message,[
                'parse_mode' => 'Markdown'
            ]);
        
        } else {
                
            $bot->reply("Whoops, tidak ada nama barang itu disini, cek kembali input anda!",[
                'parse_mode' => 'Markdown'
            ]);

        }

        

    }
    
}
