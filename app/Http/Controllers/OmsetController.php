<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nota;
use BotMan\BotMan\BotMan;

use Carbon\Carbon;

class OmsetController extends Controller
{
    public static function getData()
    {
        config(['app.locale' => 'id']);
        Carbon::setLocale('id');
        $tvar = array("keramik","cat","sanitary","building","hardware","lainlain","ongkir","ongkar");

        $omsetperjam = [];
        foreach ($tvar as $kategori) {
            $omsetperkategori = Nota::where('user','like','kasir%')
                                    ->where('remarks','not like','RTJ%')
                                    ->where('kategori',$kategori)
                                    ->whereDate('timestamp',Carbon::now())
                                    ->where('timestamp', '<=', Carbon::now())
                                    ->where('timestamp', '>=', Carbon::now()->subHours(1))
                                    ->get();

            $omsetperjam[$kategori] = $omsetperkategori->sum('total');
        }

        $returperjam = [];
        foreach ($tvar as $kategori) {
            $retur = Nota::where('user','like','kasir%')
                                    ->where('remarks','like','RTJ%')
                                    ->where('kategori',$kategori)
                                    ->whereDate('timestamp',Carbon::now())
                                    ->where('timestamp', '<=', Carbon::now())
                                    ->where('timestamp', '>=', Carbon::now()->subHours(1))
                                    ->get();

            $returperjam[$kategori] = $retur->sum('total');
        }


        $omsettotal = [];
        foreach ($tvar as $kategori) {
            $omset = Nota::where('user','like','kasir%')
                                    ->where('remarks','not like','RTJ%')
                                    ->where('kategori',$kategori)
                                    ->whereDate('timestamp',Carbon::now())
                                    ->get();

            $omsettotal[$kategori] = $omset->sum('total');
        }

        $returtotal = [];
        foreach ($tvar as $kategori) {
            $retur = Nota::where('user','like','kasir%')
                                    ->where('remarks','like','RTJ%')
                                    ->where('kategori',$kategori)
                                    ->whereDate('timestamp',Carbon::now())
                                    ->get();

            $returtotal[$kategori] = $retur->sum('total');
        }

        $gt = 0;
        $gt2 = 0;
        // Carbon::setLocale('id');
        //dd($omsetperjam);
        $text = "*Laporan Penjualan Kasir - ASNET*\n\n";
        $text .= Carbon::now()->translatedFormat('l, d F Y')."\nJam ".Carbon::now()->subHours(1)->format('H:i')." s/d ".Carbon::now()->format('H:i');

        $text .= "\n\n💰 Penjualan 💰 ";
        $text .= "\n----\n```\n";
        foreach ($tvar as $kategori) {
        
            $text .= str_pad($kategori, 8)." : Rp ".str_pad(number_format($omsetperjam[$kategori],0,',','.'), 11, " ", STR_PAD_LEFT).",-\n"; 
        }

        $text .= "```\nRetur ";
        $text .= "\n----\n```\n";
        $gtreturperjam = 0 ;
        foreach ($tvar as $kategori) {
            if ($returperjam[$kategori] != 0 ) {
                $text .= str_pad($kategori, 8)." : Rp ".str_pad(number_format($returperjam[$kategori],0,',','.'), 11, " ", STR_PAD_LEFT).",-\n";
            }
            $gtreturperjam += $returperjam[$kategori];
        // $text .= $kategori.": Rp ".number_format($returperjam[$kategori],2,',','.')."\n"; 
                    
        }
        if ($gtreturperjam == 0 ) {
            $text .= "``` Tidak ada retur di jam ini.\n\n";
        } else {
            $text .= "```\n\n";
        }

        $text .= "✅📊 Omset Harian 📊✅";
        $text .= "\n---- ``` \n";
        foreach ($tvar as $kategori) {
            
            $text .= str_pad($kategori, 8)." : Rp ".str_pad(number_format($omsettotal[$kategori],0,',','.'), 11, " ", STR_PAD_LEFT).",-\n";
        // $text .= $kategori.": Rp ".number_format($omsettotal[$kategori],2,',','.')."\n"; 
            $gt = $gt +  $omsettotal[$kategori] ;       
        }
        $text .= "```\nTotal Retur";
        $text .= "\n----\n```\n";
        $gtreturperjam2 = 0 ;
        foreach ($tvar as $kategori) {

            if ($returtotal[$kategori] != 0 ){
                $text .= str_pad($kategori, 8)." : Rp ".str_pad(number_format($returtotal[$kategori],0,',','.'), 11, " ", STR_PAD_LEFT).",-\n";
            }
            $gtreturperjam2 += $returtotal[$kategori];
            
            //$text .= $kategori.": Rp ".number_format($returtotal[$kategori],2,',','.')."\n"; 
            $gt2 = $gt2 +  $returtotal[$kategori] ;        
        }
        if ($gtreturperjam2 == 0 ) {
            $text .= "``` Tidak ada retur di jam ini.\n";
        } else {
            $text .= "```\n";
        }

        $text .= "\n=========================== ``` ";
        $text .= "\nTotal    : Rp ".str_pad(number_format($gt,0,',','.'), 11, " ", STR_PAD_LEFT).",-";
        $text .= "\nRetur    : Rp ".str_pad(number_format($gt2,0,',','.'), 11, " ", STR_PAD_LEFT).",-";
        $text .= "\nGT       : Rp ".str_pad(number_format($gt+$gt2,0,',','.'), 11, " ", STR_PAD_LEFT).",-";
        $text .= "```\n===========================";

        // dd($text);

        // $user = $bot->getUser();
        // $firstname = $user->getFirstName();
        

        return $text;
    }

    public static function omset($bot)
    {
        $text = self::getData();

        $data = json_decode(json_encode($bot->getMessage()->getPayload()),false);
        
        $bot->reply($text,[
            'reply_to_message_id' => $data->message_id,
            'parse_mode' => 'Markdown'
        ]);
        
    }

    public static function omsethourly()
    {
        $text = $this->getData();

        // $data = json_decode(json_encode($bot->getMessage()->getPayload()),false);
        
        $bot->say($text,[
            'chat_id' => config('telegram.groupid'),
            'parse_mode' => 'Markdown'
        ],TelegramDriver::class);
        
    }
}
