<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;

class HargaController extends Controller
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        //parent::__construct();

		$this->dblocation = Config::get('asnet.dblocation');
		$this->dbname = Config::get('asnet.dbname');
	}
	public function cek($pencarian)
	{
		
		//$dblocation = Config::get('asnet.dblocation');
		//$dbname = Config::get('asnet.dbname');
		
		
		$conn = new \COM("ADODB.Connection");        
					try {

						$filename = $this->dblocation.$this->dbname;
						if (file_exists($filename)) {
							$lastupdate = "Data dari ".$this->dbname." Server ".Config::get('app.name')." ``` \nLast modified: " . date ("d F Y H:i:s.", filemtime($filename))."```";
						}

						$conn->Open("Provider=vfpoledb.1;Data Source=".$this->dblocation.";Deleted=true;Collating Sequence=MACHINE");
						if (! $conn) {
							throw new Exception("Could not connect!");
						}
					}
					 catch (Exception $e) {
						 echo "Error (File:): ".$e->getMessage()."<br>";
					}        
					
					$pencarian = (string)trim($pencarian);
					$pencarian = str_replace(' ', '', $pencarian );
					
					$postkode = strtoupper($pencarian) ;
				
				
				$sql = "SELECT * from ".$this->dbname." WHERE STRTRAN(KODE_STOCK, ' ', '') = '". $postkode ."' OR BARCODE = '". $postkode."';" ;

				//echo $ma_sql."<br>" ; 

				$rs = $conn->Execute($sql);
				$x = 0;
				while (!$rs->EOF) {
				
					$kode 		= trim((string)$rs->Fields("KODE_STOCK")," \t");
                    $nama		= trim((string)$rs->Fields("NAMA")," \t");
                    $satuan		= trim((string)$rs->Fields("SATUAN")," \t");
                    $rak	    = trim((string)$rs->Fields("RAK")," \t");
					//$saldo	= (int)$rs->Fields("SALDO_AKH") ;
                    $saldo		= (int)$rs->Fields("S_AKHIR") ;
                    $hargajual 	= (string)$rs->Fields("HARGAJUAL") ;
					$barcode 	= trim((string)$rs->Fields("BARCODE"));
					
					$result = (object)['id' => $x++ , 
										'kode' 		=> $kode, 
										'barcode' => $barcode,	
										'nama'		=> $nama,		
										'satuan'	=> $satuan,	
										//'rak'		=> $rak,	
										'saldo'		=> $saldo,	
										'hargajual' => $hargajual 
									];
					
					
					//echo 	$rs->Fields("KODE_STOCK");
						
					
					
					$rs->MoveNext();
					
				}
				//Close connections
				$rs->Close();
				$conn->Close();
				$rs = null;
				$conn = null;
				
				if (!empty($kode)) {
					
						$results = (object)[
							'type' => 'success',
							'update' => $lastupdate,
							'value' => (object)$result,
						];
					return $results;
				
				} else {
					$results = (object)[ 'type' => 404, 'message' => 'Not found' ];
					return $results;
				}
				//return response()->json($data);
	}

	public function search($pencarian)
	{
		$conn = new \COM("ADODB.Connection");        
					try {

						$filename = $this->dblocation.$this->dbname;
						
						if (file_exists($filename)) {
							$lastupdate = "Data dari \"".$this->dbname."\" Server ".Config::get('app.name')." ``` \nLast modified: " . date ("d F Y H:i:s.", filemtime($filename))."```";
							//$lastupdate = $filename;
						} else {
							$lastupdate = $filename;
						}
						$dbconn = 'Provider=VFPOLEDB.1;Data Source='.$this->dblocation.';Deleted=true;Collating Sequence=MACHINE';
						$conn->Open($dbconn);
						if (! $conn) {
							throw new Exception("Could not connect!");
						}
					}
					 catch (Exception $e) {
						 echo "Error (File:): ".$e->getMessage()."<br>";
					}          
				
				$pencarian = trim($pencarian);
				$queryawal = $pencarian;

				if (strpos($pencarian, ' ') !== false) {
					// echo 'has spaces, but not at beginning or end';
					$pencarian = str_replace(' ', '%', $pencarian );
				}	

				
               
                $pencarian = str_replace('*', '%', $pencarian );
				$pencarian = str_replace('-', '%', $pencarian );
				$postkode = $pencarian ;
                $postkode2 = strtoupper($pencarian) ;
				
				
				$ma_sql = "SELECT * from ".$this->dbname." WHERE NAMA LIKE '%". $postkode ."%' OR NAMA LIKE '%". $postkode2 ."%';" ;

				//echo $ma_sql."<br>" ; 
                $x=0;
				$rs = $conn->Execute($ma_sql);
				
				while (!$rs->EOF) {
				
                        
                    $kode 		= trim((string)$rs->Fields("KODE_STOCK")," \t");
                    $nama		= trim((string)$rs->Fields("NAMA")," \t");
                    $satuan		= trim((string)$rs->Fields("SATUAN")," \t");
                    $rak	    = trim((string)$rs->Fields("RAK")," \t");
					//$saldo	= (int)$rs->Fields("SALDO_AKH") ;
                    $saldo		= (int)$rs->Fields("S_AKHIR") ;
                    $hargajual 	= (string)$rs->Fields("HARGAJUAL") ;
                    $barcode 	= trim((string)$rs->Fields("BARCODE"));
                    
                        $result[]           = (object)['id' => $x++ , 
                                                'kode' 		=> $kode, 
                                                'barcode' => $barcode,	
                                                'nama'		=> $nama,		
                                                'satuan'	=> $satuan,	
                                                //'rak'		=> $rak,	
                                                'saldo'		=> $saldo,	
                                                'hargajual' => $hargajual 
                                            ];
					
					//echo 	$rs->Fields("KODE_STOCK");
						
					
					
					$rs->MoveNext();
					
				}
				//Close connections
				$rs->Close();
				$conn->Close();
				$rs = null;
				$conn = null;
				
				if (!empty($kode)) {
					// $collection = collect($results)->map(function ($hasil) {
					// 	return $hasil;
					// });
					$results = (object)[
						'type' => 'success',
						'count' => $x,
						'query' => $queryawal,
						'update' => $lastupdate,
                        'value' => $result,
                    ];
					return $results;
				
				} else {
					$results = (object)[ 'type' => 404, 'message' => 'Not found' ];
					return $results;
				}
				//return response()->json($data);
	}

	
}
