<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use App\Conversations\OnboardingConversation;

class BotManController extends Controller
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        //parent::__construct();

        $this->botman = app('botman');
    }

    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        info('incoming', request()->all()); // this line was added

        $botman = app('botman');

        $botman->listen();

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        //info('incoming', request()->all()); // this line was added
        return view('tinker');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function send()
    {
        
        return $this->botman->say("Hello I'm Testbot", '498456318', TelegramDriver::class);
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new OnboardingConversation());
    }
}
