<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    protected $connection = 'mysql-kasir';
    protected $table = 'nota';
}
